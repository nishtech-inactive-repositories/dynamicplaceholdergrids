# README #

This is a sample implementation of a Bootstrap Grid System using new Sitecore 9 Dynamic Placeholders. The whole working merchanism of this solution is fully described at the article "Bootstrap Grid System with Sitecore 9 Dynamic Placeholders" (http://www.nishtechinc.com/Blog/2018/February/Bootstrap-Grid-System-with-Sitecore-9-Dynamic-Placeholders)

### What is this repository for? ###

* A Bootstrap-compatible Grid System for Sitecore 9
* [Details here](http://www.nishtechinc.com/Blog/2018/February/Bootstrap-Grid-System-with-Sitecore-9-Dynamic-Placeholders)

### Who do I talk to? ###

* Rodrigo Peplau - rpeplau@nishtechinc.com
